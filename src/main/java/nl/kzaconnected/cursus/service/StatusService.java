package nl.kzaconnected.cursus.service;

import nl.kzaconnected.cursus.model.Dao.Status;
import nl.kzaconnected.cursus.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusService {

    @Autowired
    private StatusRepository statusRepository;

    public List<Status> findAll(){
        return statusRepository.findAll();
    }
}
